import bs4
import requests
import re
import json
import time


def get_number(text):
    return re.findall('\d+', text )[0]

def refine_text(text):
    return text.replace('\n','')

def get_page_content(url):
   page = requests.get(url,headers={"Accept-Language":"en-US"})
   return bs4.BeautifulSoup(page.text.strip(),"html.parser")


def get_basic_info(url):
    soup = get_page_content(url)

    books = soup.findAll('a', class_='bookTitle')
    links = [book['href'] for book in books]
    title = [book.text.strip() for book in books]
    book_id = [get_number(book['href']) for book in books]
    authors_container = soup.findAll('span', itemprop='author')
    authors = [item.findAll('a','authorName') for item in authors_container]
    final_authors = []
    for item in authors:
        li = [refine_text(ite.find('span').text.strip()) for ite in item] 
        final_authors.append(li)
    
    return book_id,links,title,final_authors 

def get_book_rating(questions):
    rating = refine_text(questions.findAll('span',itemprop ='ratingValue')[0].text)
    return rating

def get_description(questions):
    try:
        desc = questions.findAll('div',class_ = 'readable stacked')[0].text.split('\n')
    except:
        print("Description is empty")
        return None
    if len(desc) <= 3:
        desc = refine_text(desc[1])
    else:
        desc = refine_text(desc[2])
    return desc

def get_user_rate(questions):
    user_rate = [item.text.strip() for item in questions.findAll('span',class_='staticStars notranslate')]
    return user_rate

def get_date_post(questions):
    date = [item.text.strip() for item in questions.findAll('a',class_='reviewDate createdAt right')]
    return date

def get_reviews_each_book(questions):

    # Get Ajax request
    # for a in questions.select('a[onclick*="new"]'):
    #     print(a['onclick'])


    reviews = []
    review_content = questions.findAll('div',class_ = 'reviewText stacked')

    for item in review_content:
        review = item.text.split('\n')
        # print(len(review))
        if len(review) <= 5:
            review = refine_text(review[2])
        else:
            review = refine_text(review[3])
        reviews.append(review)

    return reviews

def get_comments_each_book(PATH,questions):
    see_review_bar = questions.findAll('div', class_ = 'updateActionLinks')
    link_comments = [item.findAll('a') for item in see_review_bar ]
    link_comments = [item[-1]['href'] for item in link_comments ]

    comments = []
    for cm_link in link_comments:
        comment_questions = get_page_content(PATH + cm_link)

        all_comments = comment_questions.findAll('div' , class_ = 'mediumText reviewText')
        all_comments = [refine_text(item.text.strip()) for item in all_comments]

        comments.append(all_comments)

    return comments

def crawl(url):
    print("Crawling data ...")
    start = time.time()

    book_id,links,title,final_authors = get_basic_info(url)

    PATH = 'https://www.goodreads.com'

    book_desc = []
    id_users = []
    name_users = []
    user_rates = []
    rate = []
    review_contents = []
    list_comments = []
    date_post = []

    for link in links:
        questions = get_page_content(PATH + link)
        # rating
        rating = get_book_rating(questions)
        rate.append(rating)
        # description
        desc = get_description(questions)
        book_desc.append(desc)

        # Review list
        users = questions.findAll('a',class_='user')
        # user name
        user_name = [refine_text(item['title']) for item in users]
        name_users.append(user_name)
        # user id
        id_user = [get_number(item['href']) for item in users]
        id_users.append(id_user)
        # user rating
        user_rate = get_user_rate(questions)
        user_rates.append(user_rate)
        # date post
        date = get_date_post(questions)
        date_post.append(date)

        # reviews content
        reviews = get_reviews_each_book(questions)
        review_contents.append(reviews)

        # list comment of each review
        comments = get_comments_each_book(PATH,questions)
        list_comments.append(comments)
        
    print("Finish get all info :", time.time() -start)

    return rate,book_desc,id_users,name_users,user_rates,date_post,review_contents,list_comments


# ID Book [x]
# • Link Book [x]
# • Title Book [x]
# • Author Book [x]
# • Rate Book [x]
# • Description Book [x]
# • Reviews Book
    # • ID User [x]
    # • Name User [x]
    # • Rate [x]
    # • Review content [x]
    # • List comments
    # • Date post [x]

if __name__ == '__main__':
    
    MAX_PAGE = 100
    SAVING_PATH = "database_"

    base_url = 'https://www.goodreads.com/author/list/4634532.Nguy_n_Nh_t_nh?page=1&per_page=30' # các bạn thay link của trang mình cần lấy dữ liệu tại đâydef get_page_content(url):
    
    for i in range(2,MAX_PAGE):
        url = 'https://www.goodreads.com/author/list/4634532.Nguy_n_Nh_t_nh?page={}&per_page=30'.format(i) 

        book_id,links,title,final_authors = get_basic_info(url)

        if not title:
            print("Page does not exist")
            break

        rate,book_desc,id_users,name_users,user_rates,date_post,review_contents,list_comments = crawl(url)
        

        data = {
            'books_id' : book_id,
            'link_book' : links,
            'book_title' : title,
            'author' : final_authors,
            'book_rate' : rate,
            'description' : book_desc,
            'reviews_book' : {
                'user_id' : id_users,
                'user_name' : name_users,
                'user_rate' : user_rates,
                'date_post' : date_post,
                'review_content' : review_contents,
                'list_comment' : list_comments
            }
        }

        print('Saving to {}'.format(SAVING_PATH+'{}.json'.format(i)))
        with open(SAVING_PATH + '{}.json'.format(i), 'w', encoding='utf-8') as f:
            json.dump(data, f, ensure_ascii=False, indent=4)
        